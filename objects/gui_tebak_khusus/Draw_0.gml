/// @description
draw_sprite(spr_background_2, 0, x_middle, y_middle);

if(state_id == 0){
	draw_sprite_ext(spr_white_back, 0, x_middle, y_middle, 1, 0.75, 0, image_blend, 1);
}else{
	draw_sprite(spr_white_back, 0, x_middle, y_middle+80);
	draw_text_outline_auto(x_middle, y_middle-570, string(correct_count)+"/"+string(song_count), c_black, c_black, 1, 1, 1, 600, 0, 0);
	for(var i=0; i<lifes; i++){
		draw_sprite_ext(spr_tone, 0, x_middle-150+(i*150), y_middle-430, 1, 1, 0, image_blend, 1);
	}
	
	draw_sprite_part_ext(spr_rectangle_gradient, 0, 0, 0, 549*(txt_time/60), 43, x_middle-274.5, y_middle-320, 1, 1, image_blend, 1);
	draw_text_outline_auto(x_middle, y_middle-298, string(txt_time), c_black, c_black, 1, 1, 1, 100, 0, 0);
}