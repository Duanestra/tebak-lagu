/// @description switch singer
switch(singer){
	case "ebietgade":{
		questions_string = startup.ebietgade_string;
		break;
	}
	case "koesplus":{
		questions_string = startup.koesplus_string;
		break;
	}
	case "tettykadi":{
		questions_string = startup.tettykadi_string;
		break;
	}
}

q_map = json_decode(questions_string);
if(!is_undefined(q_map)){
	var index = 1;
	var value = ds_map_find_value(q_map, string(index));
	all_options_string = string(ds_map_find_value(q_map, "options"));
	
	while(!is_undefined(value)){
		ds_list_add(q_list, value);
		ds_list_mark_as_map(q_list, ds_list_size(q_list)-1);
		value = ds_map_find_value(q_map, string(++index));
	}
}

song_count = ds_list_size(q_list);

event_user(1);