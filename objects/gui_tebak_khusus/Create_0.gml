/// @description

singer = "";
closed = true;

questions_string = "";

//buttons
b_back = noone;
b_choice = noone;
b_play_again = noone;
b_fifty = noone;
b_pass = noone;

x_middle = 360;
y_middle = 640;

txt_time = 0;;
//time_counter = 0;
//time_minutes = 0;
//time_seconds = 0;
//time_miliseconds = 0;
draw_cd = false;

correct_count = 0;
song_count = 0;

t_question = 0;
t_title = "";

fifty_used = false;
pass_used = false;

sound_id = noone;

action_id = -1;

lifes = 3;

q_map = ds_map_create();
q_list = ds_list_create();
all_options_list = ds_list_create();
all_options_string = "";

elements_layer_create();