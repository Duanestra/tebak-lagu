/// @description state engine
switch(state_id){
	case 0:{
		alarm[3] = 1; //timer up
		if(!instance_exists(b_back)){
			b_back = createButtonTargeted(80, 80, elements_layer, btn_parent, id, 0, "", "", "", spr_back_button);
		}
		
		var cera = ceil(song_count/10);
		txt_time = 30*cera;
		
		state_id = 1;
		event_user(1);
		break;
	}
	case 1:{ //main gameplay
		repeat(5){
			ds_list_shuffle(q_list);
		}
		
		draw_cd = false;
		
		if(ds_list_size(q_list) > 0){
			t_question = ds_list_find_value(q_list, 0);
		}else{
			show_debug("missing song data.. back to Main Screen");
			event_user(0); //back to first screen
			exit;
		}
		
		t_title = ds_map_find_value(t_question, "s_title");
		sound_id = asset_get_index(t_title);
		
		audio_play_sound(sound_id, 1, 0);
		
		var correct_answer = ds_map_find_value(t_question, "s_options");
		var all_options_arr = scp_split_string(all_options_string, ",");
		
		scp_convert_array_to_list(all_options_arr, all_options_list); //convert array to become a list
		
		var opt_list = ds_list_create(); //here is the 4 options for the game
		var randomer = irandom_range(0, array_length(all_options_arr)-1);
		ds_list_add(opt_list, correct_answer);
		while(ds_list_size(opt_list) < 4){
			if(ds_list_find_index(opt_list, ds_list_find_value(all_options_list, randomer)) == -1){
				//if the option is not yet in the list, add it
				ds_list_add(opt_list, ds_list_find_value(all_options_list, randomer));
			}else{
				// if the option is already in the list, randomize again
				randomer = irandom_range(0, array_length(all_options_arr)-1);
				show_debug("Want to add choice but already exists.. randomize again");
			}
		}
		
		repeat(5){
			//shuffle the options
			ds_list_shuffle(opt_list);
		}
		
		for(var i=0; i<4; i++){
			b_choice[i] = createButtonTargeted(x_middle, 480+(i*115), elements_layer, btn_choice, id, 2, string(ds_list_find_value(opt_list,i)), "", "", spr_rectangle_2);
			with(b_choice[i]){
				scale_text = 0.9;
				text_compare = other.t_title;
				text_compare = string_replace_all(text_compare, "_", " ");
				text_compare = string_replace_all(text_compare, " intro", "");
				if(string_lower(text) == string_lower(text_compare)){
					action_id = 1;
				}else{
					action_id = 2;
				}
			}
		}
		
		b_play_again = createButtonTargeted(x_middle+222, y_middle+320, elements_layer, btn_parent, id, 2, "Putar\nLagi", "", "", spr_rectangle_3);
		with(b_play_again){
			action_id = 0;
			f_c = c_black;
		}
		
		b_fifty = createButtonTargeted(x_middle, y_middle+320, elements_layer, btn_parent, id, 2, "50:50", "", "", spr_rectangle_3);
		with(b_fifty){
			if(other.fifty_used){
				can_click = 0;
				image_blend = c_gray;
				_target_event = -1;
			}
			action_id = 3;
			f_c = c_black;
			restore_blend = false;
		}
		
		b_pass = createButtonTargeted(x_middle-222, y_middle+320, elements_layer, btn_parent, id, 2, "PASS", "", "", spr_rectangle_3);
		with(b_pass){
			if(other.pass_used){
				can_click = 0;
				image_blend = c_gray;
				_target_event = -1;
			}
			action_id = 4;
			f_c = c_black;
			restore_blend = false;
		}
		
		with(btn_parent){
			text_yoff = -10;
			out_line_px = 0;
		}
		
		ds_list_clear(all_options_list);
		ds_list_destroy(opt_list);
		break;
	}
	case 2:{ //destroy choices and go to next song
		instance_destroy(b_pass);
		instance_destroy(b_fifty);
		instance_destroy(b_play_again);
		instance_destroy(btn_choice);
		state_id = 1;
		event_user(1);
	}
}