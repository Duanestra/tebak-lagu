//show_debug(object_get_name(object_index) + " left pressed");
if keyboard_virtual_status() == true{ exit; }
if(visible) {
	for(var i = 0; i < array_length_1d(ignore_click_arr); i++){
		if(instance_exists(ignore_click_arr[i]) && anti_ignore_click_arr = false){
			show_debug("btn_parent caught clickthrough: " + string(object_get_name(ignore_click_arr[i].object_index)));
			exit;
		}
	}
    if(is_imploding == true) { exit; }
	
    if(can_click == 1 && !ScheduleExists(a_sch) && was_pressed) {
        can_click = 0;
        show_debug("left released " + object_get_name(object_index) + " with id " +  string(id));
        //sprite_index = sprite;
 
        b_sch = ScheduleAlarm(id,1,p_lock_delay,1);
        a_sch = ScheduleAlarm(id,1,p_effect_time*0.5,0);
    }
}

press_pressed_time = current_time - press_start_time;

with(btn_parent){
	was_pressed = false;
}