//show_debug(object_get_name(object_index) + " left pressed");
if keyboard_virtual_status() == true{ exit; }
if(is_imploding == true) { exit; }
if(can_click == 0) { exit; }

if(blend_change){
	image_blend = c_gray;
}
if(restore_scale){
	image_xscale = 0.9;
	image_yscale = 0.9;
}

was_pressed = true;
press_start_time = current_time;
press_pressed_time = 0;