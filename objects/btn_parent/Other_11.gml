/// @description Pulse effect
if(do_pulse == 0) { exit; }

if(!can_click && do_pulse == 1){ p_sched = ScheduleEventUser(id, true, p_time, p_event); exit; }

if(do_pulse == 1) {
    p_sched = ScheduleEventUser(id, true, p_time, p_event);
}
