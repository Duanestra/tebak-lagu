/// @description Destroy Self
if(is_imploding == false) {
    is_imploding = true;
    
    ScheduleScript(id, true, ui_time+0.05, destroy_instance, id);
}

