/// @description Setup

can_click = 0;
alarm[1] = room_speed/5;
text = "";
press = sprite_index;
sprite = sprite_index;

restore_blend = true;
restore_scale = true;
blend_change = true;

image_speed = 0;
image_index = 0;
ui_time = global.g_ui_time;

anti_ignore_click_arr = false;

b_c = c_black;
f_c = c_white;
fn_valign = fa_middle;
fn_halign = fa_center;
m_draw_font = fnt_normal;
text_yoff = 0;
text_xoff = 0;
icon_xoff = 0;
icon_yoff = 0;
force_max = 1000; //Default to not used
draw_spr = true;
draw_texts = true;

a_sch = ScheduleNull();
b_sch = ScheduleNull();

icon = noone;
iconLeft = 1;
iconRight = 1;

icon_xscale = 1;
icon_yscale = 1;

base_xscale = 1;
base_yscale = 1;

is_imploding = false;

_target = noone;
_target_event = 0;
action_id = noone;

_parent = noone;
_parent_event = 0;

//Pulse Effect
already_nuked = 0;
do_pulse = 1;
p_sched = ScheduleNull();

p_event = 3; 
p_time = 5;

//Press Effect - Set start == dest to disable an effect
p_xscale_start = 1;
p_yscale_start = 1;

p_effect_time = 0.2;

//Wiggle
w_time = 0.1; //Interval between tween

//Lock Delay - prevents repushes for set time
p_lock_delay = p_effect_time * 0.5;

oscp_range_nerf = 30; //How much space from edge of sprite to end of text

tween_mode = 0;//0 pulse // 1 wiggle // 2 bounce // 3 shake // 4 bounce sml
do_loop = 1;
cr_mode = 0; // 0 fade, // 1 pop // 2 slide-left // 3 slide-right // 4 slide up

//Outline control
out_line_px = 2;
soft_outline = true;

max_text_scale = 1.0;

use_nineslice = false;
width = 0;
height = 0;

//Finish Create 1 step later for create time configs to be accounted for. 
alarm[11] = 1;

surface_sprite = -1;

//Work around for sprite offset getting pushed off
did_explode = false;

was_pressed = false;

// text scale control
scale_text = 1;

ignore_click_arr = [];

press_start_time = 0;
press_pressed_time = 0;

follow_parent = false;
offset_x = 0;
offset_y = 0;
read_offset = false;