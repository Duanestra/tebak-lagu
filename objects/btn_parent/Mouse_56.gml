/// @description restore button
if keyboard_virtual_status() == true{ exit; }
if(restore_blend){
	image_blend = c_white;
}
if(restore_scale){
	image_xscale = 1;
	image_yscale = 1;
}