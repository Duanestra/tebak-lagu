/// @description Insert description here
// You can write your code in this editor
scp_fade_back(0.8, c_black, 1);
draw_self();

draw_set_font(fnt_fun);
draw_text_outline(x - 80, y-370, "Nama", c_black, c_white, 1.3, 1.3, 1, 0);
draw_text_outline(x + 220, y-370, "Skor", c_black, c_white, 1.3, 1.3, 1, 0);

if(leaderboard_ready){
	var img_indx = 1;
	
	for(var i=0; i<limit; i++){
		if(lb_username[i] = global.player_name){ img_indx = 0;}
		else{ img_indx = 1;}
		
		draw_sprite_ext(spr_rectangle_round_white, img_indx, x, y-293+(i*60), 1, 1.25, 0, image_blend, 1);
		
		draw_set_halign(fa_left);
		draw_text_outline(x-300, y-300+(i*60), string(i+1), c_black, c_black, 1, 1, 1, 0);
		draw_text_outline(x-250, y-300+(i*60), lb_username[i], c_black, c_black, 1, 1, 1, 0);
	
		draw_set_halign(fa_center);
		draw_text_outline(x+220, y-300+(i*60), lb_score[i], c_black, c_black, 1, 1, 1, 0);
		if(i < 1){ //crown
			draw_sprite_ext(spr_crown, 0, x+280, y-303, 0.6, 0.6, 0, image_blend, 1);
		}
	}
}