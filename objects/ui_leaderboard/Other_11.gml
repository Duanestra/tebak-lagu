var def_map = json_decode(string_data);
var first_find = ds_map_find_first(def_map);
var leaderboard_list = ds_map_find_value(def_map, first_find);
var lb_size = ds_list_size(leaderboard_list);
limit = limit >  lb_size ? lb_size : limit;

for(var i=0; i<limit; i++){
	leaderboard_map = ds_list_find_value(leaderboard_list, i);
	lb_username[i] = ds_map_find_value(leaderboard_map, "username");
	lb_score[i] = ds_map_find_value(leaderboard_map, "highscore");
}

leaderboard_ready = true;