/// @description clean and destroy

//save player data for offline need, case no username but has highscore
scp_save_player_data_offline();

//clean and destroy
if keyboard_virtual_status() == true{
	keyboard_virtual_hide();
}

if(instance_exists(ui_popup_box)){ instance_destroy(ui_popup_box) }
if(instance_exists(obj_text_input_display)){ instance_destroy(obj_text_input_display) }
if(instance_exists(ui_popup_username_check)){ instance_destroy(ui_popup_username_check) }