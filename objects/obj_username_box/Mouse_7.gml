/// @description show/hide keyboard
if(instance_exists(ui_popup_box)){ exit; }

if(os_is_network_connected(true)){ //need Online to access
	if keyboard_virtual_status() == false{
		if(global.player_name == ""){
		    keyboard_string = string_copy(keyboard_string, 1, limit);
		    keyboard_virtual_show(kbv_type_default, kbv_returnkey_default, kbv_autocapitalize_none, false);
		
			instance_create_layer(x, y_middle+100, elements_layer, obj_text_input_display);
			var pops = instance_create_layer(x, y_middle-150, elements_layer, ui_popup_box);
			with(pops){
				title_text = "PERHATIAN!";
				body_text = "Namamu akan tampil di Peringkat. Kamu hanya\nbisa mengisi namamu satu kali, setelah\nitu kamu tidak bisa menggantinya."
			}
		}
	}
}else{
	action_id = 1;
	ScheduleEventUser(id,true,0.05,5);
}