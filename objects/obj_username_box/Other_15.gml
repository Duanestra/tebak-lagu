/// @description callbacks
switch(action_id){
	case 1:{
		var pops = instance_create_layer(x, y_middle-150, front_layer, ui_popup_box);
		with(pops){
			title_text = "PERHATIAN!";
			body_text = "Kamu harus ONLINE untuk mengisi nama."
			can_close = true;
		}
		break;
	}
}