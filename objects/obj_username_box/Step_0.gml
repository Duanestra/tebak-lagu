/// @description
if keyboard_virtual_status(){
	if(string_length(keyboard_string) > limit){
		keyboard_string = string_copy(keyboard_string, 1, limit);
	}
	global.player_name = keyboard_string;
	
	if(keyboard_check_released(vk_enter)){
		event_user(3);
	}
}