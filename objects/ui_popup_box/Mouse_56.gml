/// @description 
if(can_close){
	can_close = false;
	instance_destroy(id);
}