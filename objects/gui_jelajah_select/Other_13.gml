/// @description mode switch
if(modes){
	modes = 0;
	with(btn_selection){
		sprite_index = spr_music_icon;
		sprite = spr_music_icon;
	}
	with(b_changer){
		image_index = 0;
	}
}else{
	modes = 1;
	with(btn_selection){
		sprite_index = spr_trophy;
		sprite = spr_trophy;
	}
	with(b_changer){
		image_index = 1;
	}
}