/// @description spawn buttons
if(!instance_exists(b_back)){
	b_back = createButtonTargeted(80, 80, elements_layer, btn_parent, id, 0, "", "", "", spr_back_button);
}

if(!instance_exists(b_changer)){
	b_changer = createButtonTargeted(x_middle, y_middle-400, elements_layer, btn_parent, id, 3, "", "", "", spr_btn_switch);
	with(b_changer){
		blend_change = false;
		restore_scale = false;
	}
}

var indexes = 0;
for(var i=0; i<4; i++){ //row count
	for(var j=0; j<3; j++){ //column count
		var _incy = i*200;
		var _incx = j*200;
		b_buttons = createButtonTargeted(x_middle-200+_incx, y_middle-200+_incy, elements_layer, btn_selection, id, 2, "", "", "", spr_music_icon);
		with(b_buttons){
			action_id = indexes;
			text = other.text_button[indexes];
		}
		indexes++;
	}
}

if(instance_exists(btn_selection)){
	with(btn_selection){
		scale_text = 0.65;
		text_yoff = 50;
	}
}