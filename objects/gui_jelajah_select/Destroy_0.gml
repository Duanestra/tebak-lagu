/// @description
if(instance_exists(b_back)){ instance_destroy(b_back); }
if(instance_exists(b_changer)){ instance_destroy(b_changer); }
if(instance_exists(btn_selection)){ instance_destroy(btn_selection); }

if(back_to_main){
	instance_create_layer(0, 0, elements_layer, gui_first_interactive_screen);
	back_to_main = false;
}