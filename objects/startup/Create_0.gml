/// @description Insert description here
// You can write your code in this editor
scp_global();

//==================== questions load start ====================
question_file_intro = file_text_open_read("nostalgia_intro.json");
nostalgia_intro_string = "";

if(question_file_intro != -1){
	while(!file_text_eof(question_file_intro)){
		var _val = file_text_readln(question_file_intro);
		nostalgia_intro_string += _val;
	}
}else{ show_debug("ERROR file \"nostalgia_intro.json\" doesn't exist"); }

if(question_file_intro != -1){
	file_text_close(question_file_intro);
}

//==================== questions load start ====================
question_file_vokal = file_text_open_read("nostalgia_vokal.json");
nostalgia_vokal_string = "";

if(question_file_vokal != -1){
	while(!file_text_eof(question_file_vokal)){
		var _val = file_text_readln(question_file_vokal);
		nostalgia_vokal_string += _val;
	}
}else{ show_debug("ERROR file \"nostalgia_vokal.json\" doesn't exist"); }

if(question_file_vokal != -1){
	file_text_close(question_file_vokal);
}

//==================== questions load start ====================
question_ebietgade = file_text_open_read("ebietgade.json");
ebietgade_string = "";

if(question_ebietgade != -1){
	while(!file_text_eof(question_ebietgade)){
		var _val = file_text_readln(question_ebietgade);
		ebietgade_string += _val;
	}
}else{ show_debug("ERROR file \"ebietgade.json\" doesn't exist"); }

if(question_ebietgade != -1){
	file_text_close(question_ebietgade);
}

//==================== questions load start ====================
question_koesplus = file_text_open_read("koesplus.json");
koesplus_string = "";

if(question_koesplus != -1){
	while(!file_text_eof(question_koesplus)){
		var _val = file_text_readln(question_koesplus);
		koesplus_string += _val;
	}
}else{ show_debug("ERROR file \"ebietgade.json\" doesn't exist"); }

if(question_koesplus != -1){
	file_text_close(question_koesplus);
}

//==================== questions load start ====================
question_tettykadi = file_text_open_read("tettykadi.json");
tettykadi_string = "";

if(question_tettykadi != -1){
	while(!file_text_eof(question_tettykadi)){
		var _val = file_text_readln(question_tettykadi);
		tettykadi_string += _val;
	}
}else{ show_debug("ERROR file \"ebietgade.json\" doesn't exist"); }

if(question_tettykadi != -1){
	file_text_close(question_tettykadi);
}

//==================== player data load start ====================
player_data_file = file_text_open_read("player_data.json");
player_data_string = "";

if(player_data_file != -1){
	while(!file_text_eof(player_data_file)){
		var _val = file_text_readln(player_data_file);
		player_data_string += _val;
	}
}else{ show_debug("ERROR file \"player_data.json\" doesn't exist"); }

if(player_data_file != -1){
	file_text_close(player_data_file);
}

player_data_map = json_decode(player_data_string);

if(!is_undefined(player_data_map)){
	global.player_name = ds_map_find_value(player_data_map, "username");
	global.player_score = ds_map_find_value(player_data_map, "highscore");
	ds_map_destroy(player_data_map);
}

if(os_type = os_windows){
	global.player_name = "PC tester";
}
	
instance_create_layer(0, 0, layer, gui_first_interactive_screen);