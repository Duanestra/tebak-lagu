/// @description spawn buttons
if(!instance_exists(b_play)){
	b_play = createButtonTargeted(x_middle, y_middle-150, elements_layer, btn_parent, id, 2);
	with(b_play){
		sprite_index = spr_play_button;
		sprite = spr_play_button;
		action_id = 0;
		restore_scale = false;
		restore_blend = false;
		TweenFire(id, EaseLinear, 2, true, 0, 1, image_xscale__, 1, 1.2);
		TweenFire(id, EaseLinear, 2, true, 0, 1, image_yscale__, 1, 1.2);
	}
}

if(!instance_exists(b_right_arrow)){
	b_right_arrow = createButtonTargeted(x_middle+250, y_middle-150, elements_layer, btn_parent, id, 2, "", "", "", spr_arrow_page);
	with(b_right_arrow){
		action_id = 5;
	}
}

if(!instance_exists(b_left_arrow)){
	b_left_arrow = createButtonTargeted(x_middle-250, y_middle-150, elements_layer, btn_parent, id, 2, "", "", "", spr_arrow_page);
	with(b_left_arrow){
		action_id = 6;
		image_angle = 180;
		visible = false; //unvisible on create
	}
}

if(!instance_exists(b_leaderboard)){
	b_leaderboard = createButtonTargeted(x_middle, y_middle+30, elements_layer, btn_parent, id, 2, "Peringkat", "", "", spr_rectangle);
	with(b_leaderboard){
		action_id = 1;
	}
}

if(!instance_exists(b_question)){
	b_question = createButtonTargeted(x_middle, y_middle+120, elements_layer, btn_parent, id, 2, "Cara Bermain", "", "", spr_rectangle);
	with(b_question){
		action_id = 2;
	}
}

if(!instance_exists(b_info)){
	b_info = createButtonTargeted(x_middle, y_middle+210, elements_layer, btn_parent, id, 2, "Info", "", "", spr_rectangle);
	with(b_info){
		action_id = 3;
	}
}

if(!instance_exists(b_rate)){
	b_rate = createButtonTargeted(x_middle, y_middle+300, elements_layer, btn_parent, id, 2, "Beri Rating", "", "", spr_rectangle);
	with(b_rate){
		action_id = 9;
	}
}
	
with(btn_parent){
	text_yoff = -8;
}
	
if(!instance_exists(b_username)){
	b_username = instance_create_layer(x_middle, y_middle - 570, elements_layer, obj_username_box);
}

if(!instance_exists(b_exit)){
	b_exit = createButtonTargeted(x_middle, y_middle+390, elements_layer, btn_parent, id, 2, "Keluar", "", "", spr_rectangle);
	with(b_exit){
		action_id = 4;
		base_xscale = 0.75;
		base_yscale = 0.75;
		text_yoff = -3;
	}
}

with(btn_parent){
	out_line_px = 0;
	array_push(ignore_click_arr, ui_popup_box);
	array_push(ignore_click_arr, ui_leaderboard);
	array_push(ignore_click_arr, ui_how_to_play);
}