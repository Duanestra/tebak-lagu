/// @description Insert description here
// You can write your code in this editor
if(instance_exists(b_play)){ instance_destroy(b_play); }
if(instance_exists(b_question)){ instance_destroy(b_question); }
if(instance_exists(b_rate)){ instance_destroy(b_rate); }
if(instance_exists(b_leaderboard)){ instance_destroy(b_leaderboard); }
if(instance_exists(b_exit)){ instance_destroy(b_exit); }
if(instance_exists(b_username)){ instance_destroy(b_username); }
if(instance_exists(b_info)){ instance_destroy(b_info); }
if(instance_exists(b_right_arrow)){ instance_destroy(b_right_arrow); }
if(instance_exists(b_left_arrow)){ instance_destroy(b_left_arrow); }

if(want_exit){
	if(os_type != os_android){
		game_end();
	}else{
		android_game_end();
	}
}