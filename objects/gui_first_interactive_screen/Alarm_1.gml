/// @description tween left
with(b_play){
	can_click = 0;
	TweenFire(id, EaseLinear, 0, true, 0, 0.05, x__, x, x-100);
	TweenFire(id, EaseLinear, 0, true, 0, 0.1, image_alpha__, 1, 0);
}

ScheduleEventUser(id, true, 0.1, 3);