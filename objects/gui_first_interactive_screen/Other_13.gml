/// @description main button changer
with(b_play){
	can_click = 1;
	TweenFire(id, EaseLinear, 0, true, 0, 0.1, x__, other.x_middle, other.x_middle);
	TweenFire(id, EaseLinear, 0, true, 0, 0.1, image_alpha__, 0, 1);
}

switch(page){
	case 0:{
		with(b_play){
			sprite_index = spr_play_button;
			sprite = spr_play_button;
			action_id = 0;
			text = "";
		}
		break;
	}
	case 1:{
		with(b_play){
			sprite_index = spr_music_icon;
			sprite = spr_music_icon;
			action_id = 7;
			text = "Kebut Symphony";
			b_c = c_navy;
			out_line_px = 2;
		}
		break;
	}
	case 2:{
		with(b_play){
			sprite_index = spr_music_icon;
			sprite = spr_music_icon;
			action_id = 8;
			text = "Jelajah Waktu";
			b_c = c_navy;
			out_line_px = 2;
		}
		break;
	}
}