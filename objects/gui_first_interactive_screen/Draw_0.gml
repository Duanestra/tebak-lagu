/// @description Insert description here
// You can write your code in this editor
draw_sprite(spr_background, 0, x_middle, y_middle);
draw_sprite_ext(spr_label_tebak, 0, x_middle, y_middle-420, 1, 1, 0, image_blend, 1);
draw_sprite_ext(spr_label_nostalgia, 0, x_middle, y_middle-330, 1, 1, 0, image_blend, 1);

draw_set_font(fnt_normal);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);
draw_text_outline_auto(x_middle, y_middle-515, "Skor tertinggimu : "+string(global.player_score), c_black, c_white, 1, 1, 1, 600, 0, 0);