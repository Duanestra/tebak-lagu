/// @description callbacks
switch(action_id){
	case 0:{ //start the Game
		instance_create_layer(x_middle, y_middle, elements_layer, gui_tebak);
		instance_destroy(id);
		break;
	}
	case 1:{ //open leaderboard
		if(os_is_network_connected(true)){ //need Online to access
			instance_create_layer(x_middle, y_middle, front_layer, ui_leaderboard);
		}else{
			var pops = instance_create_layer(x_middle, y_middle-150, front_layer, ui_popup_box);
			with(pops){
				title_text = "PERHATIAN!";
				body_text = "Kamu harus ONLINE untuk melihat peringkat."
				can_close = true;
			}
		}
		break;
	}
	case 2:{ //open how to play
		instance_create_layer(x_middle, y_middle, front_layer, ui_how_to_play);
		break;
	}
	case 3:{ //open Info
		var info = instance_create_layer(x_middle, y_middle-150, front_layer, ui_popup_box);
		with(info){
			font_use = fnt_fun;
			title_text = "Versi Beta 0.0.1";
			body_text = "Kritik, saran, pertanyaan, atau laporan kirim ke\nduanestrastudio@gmail.com"
			can_close = true;
		}
		break;
	}
	case 4:{
		want_exit = true;
		instance_destroy(id);
		break;
	}
	case 5:{ //right arrow
		if(page < 2){
			page++;
			with(b_left_arrow){
				visible = true;
			}
			if(page == 2){
				with(b_right_arrow){
					visible = false;
				}
			}
			alarm[1] = 1; //tween left
		}
		break;
	}
	case 6:{ //left arrow
		if(page > 0){
			page--;
			with(b_right_arrow){
				visible = true;
			}
			if(page == 0){
				with(b_left_arrow){
					visible = false;
				}
			}
			alarm[2] =1; //tween right
		}
		break;
	}
	case 7:{ //sementara
		instance_create_layer(x_middle, y_middle, elements_layer, gui_jelajah_select);
		//with(gui_tebak_khusus){
		//	singer = "ebietgade";
		//	event_user(0);
		//}
		instance_destroy(id);
		break;
	}
	case 8:{ //sementara
		break;
	}
	case 9:{
		url_open_full("https://play.google.com/store/apps/details?id=com.duanestrastudio.tebaklagunostalgia", "_blank", "");
		break;
	}
	case 10:{
		break;
	}
	case 11:{
		break;
	}
}