/// @description Insert description here
// You can write your code in this editor
if(instance_exists(b_back)){ instance_destroy(b_back); }
if(instance_exists(b_pass)){ instance_destroy(b_pass); }
if(instance_exists(b_fifty)){ instance_destroy(b_fifty); }
if(instance_exists(b_play_again)){ instance_destroy(b_play_again); }
if(instance_exists(btn_choice)){ instance_destroy(btn_choice); }

audio_stop_all();

if(global.player_score < curr_score){
	global.player_score = curr_score;
}

//save player data for offline need, case no username but has highscore
scp_save_player_data_offline();

//save player data online if has username
if(global.player_name != ""){
	var _url = "https://duanestra.masuk.id/setHighscore.php?";
	var _map = ds_map_create();
	ds_map_add(_map, "Authorization", "eW95b19hZG1pbjpjNG5lZmllbGQ=");
	var str = "namaplayernya=" + global.player_name + "&skorplayernya=" + string(global.player_score);
	http_request(_url, "POST", _map, str);
	
	ds_map_destroy(_map);
}

instance_create_layer(0, 0, elements_layer, gui_first_interactive_screen);
ds_map_destroy(q_map);