/// @description

state_id = -1;
closed = true;

//buttons
b_back = noone;
b_choice = noone;
b_play_again = noone;
b_fifty = noone;
b_pass = noone;
x_middle = 360;
y_middle = 640;

txt_cd = 3;
txt_time = 0;
draw_cd = false;

curr_score = 0;

t_question = 0;
t_title = "";

fifty_used = false;
pass_used = false;

sound_id = noone;

action_id = -1;

lifes = 3;

nostalgia_intro_string = startup.nostalgia_intro_string;
nostalgia_vokal_string = startup.nostalgia_vokal_string;

q_map = ds_map_create();
q_list = ds_list_create();


q_map = json_decode(nostalgia_vokal_string);
if(!is_undefined(q_map)){
	var index = 1;
	var value = ds_map_find_value(q_map, string(index));
	
	while(!is_undefined(value)){
		ds_list_add(q_list, value);
		ds_list_mark_as_map(q_list, ds_list_size(q_list)-1);
		value = ds_map_find_value(q_map, string(++index));
	}
}

repeat(5){
	ds_list_shuffle(q_list);
}

elements_layer_create();

event_user(0);