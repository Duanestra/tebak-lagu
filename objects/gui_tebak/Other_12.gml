/// @description callbacks
if(action_id != 3){
	audio_stop_all();
}

switch(action_id){
	case 0:{ //play song
		audio_play_sound(sound_id, 1, 0);
		break;
	}
	case 1:{ //answer correct
		with(btn_choice){ can_click = 0; }
		with(b_pass){ can_click = 0; }
		with(b_fifty){ can_click = 0; }
		with(b_play_again){ can_click = 0; }
		
		audio_play_sound(snd_correct, 1, 0);
		curr_score += 10;
		state_id = 2;
		ScheduleEventUser(id, true, 0.5, 1);
		break;
	}
	case 2:{ //answer wrong
		with(btn_choice){ can_click = 0; }
		with(b_pass){ can_click = 0; }
		with(b_fifty){ can_click = 0; }
		with(b_play_again){ can_click = 0; }
		
		lifes -= 1;
		audio_play_sound(snd_wrong, 1, 0);
		if(lifes <= 0){
			ScheduleEventUser(id, true, 0.5, 0);
		}else{
			state_id = 2;
			ScheduleEventUser(id, true, 0.5, 1);
		}
		break;
	}
	case 3:{ //50:50
		fifty_used = true;
		
		with(b_fifty){
			can_click = 0;
			image_blend = c_gray;
		}
		
		while(instance_number(btn_choice) > 2){
			var irndm = irandom_range(0,3)
			with(b_choice[irndm]){
				if(string_lower(text) != string_lower(text_compare)){
					instance_destroy();
				}
			}
		}
		break;
	}
	case 4:{ //PASS
		pass_used = true;
		
		with(b_pass){
			can_click = 0;
			image_blend = c_gray;
		}
		
		state_id = 2;
		event_user(1);
		break;
	}
	case 5:{
		break;
	}
	case 6:{
		break;
	}
}