{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "teluk_bayur_intro.mp3",
  "duration": 8.926375,
  "parent": {
    "name": "Nostalgia Intro",
    "path": "folders/Sounds/Nostalgia Intro.yy",
  },
  "resourceVersion": "1.0",
  "name": "teluk_bayur_intro",
  "tags": [],
  "resourceType": "GMSound",
}