{
  "compression": 3,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "gubahanku.mp3",
  "duration": 12.00175,
  "parent": {
    "name": "Nostalgia Vokal",
    "path": "folders/Sounds/Nostalgia Vokal.yy",
  },
  "resourceVersion": "1.0",
  "name": "gubahanku",
  "tags": [],
  "resourceType": "GMSound",
}