{
  "compression": 3,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "nostalgia_sma.mp3",
  "duration": 11.37625,
  "parent": {
    "name": "Nostalgia Vokal",
    "path": "folders/Sounds/Nostalgia Vokal.yy",
  },
  "resourceVersion": "1.0",
  "name": "nostalgia_sma",
  "tags": [],
  "resourceType": "GMSound",
}