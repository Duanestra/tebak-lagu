{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "dingin_intro.mp3",
  "duration": 13.5133753,
  "parent": {
    "name": "Nostalgia Intro",
    "path": "folders/Sounds/Nostalgia Intro.yy",
  },
  "resourceVersion": "1.0",
  "name": "dingin_intro",
  "tags": [],
  "resourceType": "GMSound",
}