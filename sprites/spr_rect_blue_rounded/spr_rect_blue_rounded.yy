{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 674,
  "bbox_top": 0,
  "bbox_bottom": 77,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 675,
  "height": 78,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"1ca69ab8-edaf-4958-beca-ffdf88e2bfd6","path":"sprites/spr_rect_blue_rounded/spr_rect_blue_rounded.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1ca69ab8-edaf-4958-beca-ffdf88e2bfd6","path":"sprites/spr_rect_blue_rounded/spr_rect_blue_rounded.yy",},"LayerId":{"name":"6184b796-e2be-40b6-a5d7-4855b2c327d9","path":"sprites/spr_rect_blue_rounded/spr_rect_blue_rounded.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_rect_blue_rounded","path":"sprites/spr_rect_blue_rounded/spr_rect_blue_rounded.yy",},"resourceVersion":"1.0","name":"1ca69ab8-edaf-4958-beca-ffdf88e2bfd6","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_rect_blue_rounded","path":"sprites/spr_rect_blue_rounded/spr_rect_blue_rounded.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ea20b7f1-aac8-4382-aa46-7f4894215295","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1ca69ab8-edaf-4958-beca-ffdf88e2bfd6","path":"sprites/spr_rect_blue_rounded/spr_rect_blue_rounded.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 337,
    "yorigin": 39,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_rect_blue_rounded","path":"sprites/spr_rect_blue_rounded/spr_rect_blue_rounded.yy",},
    "resourceVersion": "1.3",
    "name": "spr_rect_blue_rounded",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"6184b796-e2be-40b6-a5d7-4855b2c327d9","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_rect_blue_rounded",
  "tags": [],
  "resourceType": "GMSprite",
}