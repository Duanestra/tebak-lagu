/// @description  ScheduleSetGroup(schedule,group)
/// @param schedule
/// @param group
function ScheduleSetGroup(argument0, argument1) {

	if (argument0)
	{
	    var _schedule = global.SGMS_MAP_SCHEDULES[? argument0];
	    _schedule[@ SGMS_SCHEDULE.GROUP] = argument1;
	}


}
