/// @description perform_event(object, user_event)
/// @function perform_event
/// @param object
/// @param  user_event
function perform_event(argument0, argument1) {

	if(argument0 != noone && !is_undefined(argument0) && instance_exists(argument0)) {
	    with(argument0) { event_user(argument1); }
	}



}
