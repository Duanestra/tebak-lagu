// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scp_fade_back(alpha_value, color, view){

	var cam = view_get_camera(view);
	var cw = room_width;
	var ch = room_height;
	var xx = 0;
	var yy = 0;
	if(cam != -1) { //View exists so use it - otherwise we default to application values
		cw = view_get_wport(cam);
		ch = view_get_hport(cam);
		xx = view_get_xport(cam);
		yy = view_get_yport(cam);
	}

	var _tv = draw_get_alpha();
	draw_set_alpha(clamp(alpha_value,0,1));
	draw_rectangle_colour(xx, yy, cw, ch, color, color, color, color, false);
	draw_set_alpha(_tv);
}