/// @description  SchedulePauseTarget(target)
/// @param target
function SchedulePauseTarget(argument0) {

	var _schedules = SharedScheduler().schedules;  
	var _index = -1;

	repeat(ds_list_size(_schedules))
	{
	    // Cache schedule
	    var _schedule = _schedules[| ++_index];
    
	    // IF schedule belongs to target
	    if (_schedule[SGMS_SCHEDULE.TARGET] == argument0)
	    {
	        SchedulePause(_schedule[SGMS_SCHEDULE.ID]);
	    }
	}


}
