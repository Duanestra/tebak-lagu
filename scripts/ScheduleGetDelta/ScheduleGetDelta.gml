/// @description  ScheduleGetDelta(schedule)
/// @param schedule
function ScheduleGetDelta(argument0) {

	if (argument0)
	{
	    var _schedule = global.SGMS_MAP_SCHEDULES[? argument0];
	    return _schedule[SGMS_SCHEDULE.DELTA];
	}


}
