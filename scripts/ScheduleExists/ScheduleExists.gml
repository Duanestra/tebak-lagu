/// @description  ScheduleExists(schedule)
/// @param schedule
function ScheduleExists(argument0) {

	return (argument0 && is_array(global.SGMS_MAP_SCHEDULES[? argument0]));


}
