// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scp_save_player_data_offline(){
//==================== save player data offline to json ====================
	var data_map = ds_map_create();
	ds_map_add(data_map, "username", string(global.player_name));
	ds_map_add(data_map, "highscore", (global.player_score));

	var str_map = json_encode(data_map);

	var player_data_file = file_text_open_write("player_data.json");
	file_text_write_string(player_data_file, str_map);
	file_text_close(player_data_file);

	ds_map_destroy(data_map);
}