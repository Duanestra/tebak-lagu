/// @description zero_or_value(value[, bool_undefined])
/// @function zero_or_value
/// @param value
/// @param [bool_undefined] 
function zero_or_value() {

	/*
		This function replicates the legacy real(value) behavior 

		A real value will be passed through
	    If a string can be converted to a real value it will be. 
		Anything else will return 0
	
		[bool_undefined] - optional parameter if enabled instead of 0 undefined will be returned
	*/


	var t_ac = argument_count;
	var t_v = "";
	var t_ev = 0;

	if(t_ac >= 1) {
		t_v = argument[0];
	}

	if(t_ac >= 2) {
		if(argument[1]) { t_ev = undefined;	}
	}

	var ts_default = "convert: " + string(t_v) + " \nreturn defualt: " + string(t_ev);

	if(t_v == "" || is_undefined(t_v)) {
		//show_debug(ts_default + " \nreason: null or undefined");
		return t_ev;
	} else {
		if(is_real(t_v)) {
			var t_ob = "global"
			if(id != 0 && instance_exists(id)) {
				t_ob = string(object_get_name(id.object_index));
			}			
			//show_debug("BUG? did not need to convert real: " + string(t_v) + " object: " + t_ob);
			//assert("BOOM");
			return real(t_v);
		} else {
			var len = string_length(t_v);	
			var t_neg = "-";
			var t_sep = ".";
			var t_valid = "01234567890"+t_neg+t_sep;
			var t_min = 1; //A number must always have at least 1 character...
			var t_isneg = false;
			var t_isdub = false;
		
			//Validate only one - or . allowed
			var tc = string_count(t_neg,t_v);
			if(tc == 1) {
				if(string_char_at(t_v, 1) == t_neg) {	
					t_min++;
					t_isneg = true;
				} else {
					//negate must be in the first postion for this to be a valid number....
					show_debug(ts_default + " \nreason: Negate position"); 
					return t_ev; 
				}
			} else if(tc > 1) { 
				show_debug(ts_default + " \nreason: Negate count"); 
				return t_ev; 
			}
		
			var tc = string_count(t_sep,t_v);
			if(tc == 1) {
				t_min++;
				t_isdub = true;
			} else if(tc > 1) { 
				show_debug(ts_default + " \nreason: decimal seperator count"); 
				return t_ev; 
			}
		
			//Walk our string and copy only valid characters to the new string, preserve order		
			var out = "";
			for (var i = 1; i <= len; i++) { 
				var cc = string_char_at(t_v, i); //1 based...
				if(string_pos(cc, t_valid)) {
					out = out + cc;	
				}
			}
		
			//Validate output length and ensure it's minimum length for negate or double 
			var t_ol = string_length(out); 
			if(t_ol < t_min) {
				//show_debug(ts_default + " \nreason: final output not long enough: " + string(out)); 
				return t_ev; 
			} else {
				//Leading and Trailing 0
				if(t_isdub) {
					var t_didit = false;
					if(t_isneg) {
						if(string_char_at(t_v, 2) == t_sep) { //"-.*" -> "-0.*"	
							out = "-0." + string_copy(out,3,t_ol-2);
							t_didit = true;
						}
					} else {
						if(string_char_at(t_v, 1) == t_sep) { //".*" -> "0.*"
							out = "0" + out;
							t_didit = true;
						}
					}
			
					if(string_char_at(t_v, t_ol) == t_sep) { //"*." -> "*.0"
						out = out + "0";
						t_didit = true;
					}
			
					if(t_didit) { show_debug("Added Leading / Trailing 0 - value: " + string(out)); }
				}
			
				//Whatever is left at this point can safely convert using real()	
				//show_debug(string(t_v) + " convert to real: " + out);
				return real(out);
			}
		}
	}


}
