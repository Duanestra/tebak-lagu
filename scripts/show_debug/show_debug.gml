/// @description show_debug(string[, debug_level]);
/// @function show_debug
/// @param string[
/// @param  debug_level]
function show_debug() {
	//gml_pragma("forceinline"); // in YYC, proccessing boost

	if( argument_count >= 1 ) {
		show_debug_message(string(argument[0]));
	}


}
