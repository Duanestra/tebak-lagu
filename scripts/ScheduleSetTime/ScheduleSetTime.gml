/// @description  ScheduleSetTime(schedule,time)
/// @param schedule
/// @param time
function ScheduleSetTime(argument0, argument1) {

	if (argument0)
	{
	    var _schedule = global.SGMS_MAP_SCHEDULES[? argument0];
	    _schedule[@ SGMS_SCHEDULE.TIME] = argument1;
	}


}
