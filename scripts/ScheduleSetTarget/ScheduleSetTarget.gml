/// @description  ScheduleSetTarget(schedule,instance)
/// @param schedule
/// @param instance
function ScheduleSetTarget(argument0, argument1) {

	if (argument0)
	{
	    var _schedule = global.SGMS_MAP_SCHEDULES[? argument0];
	    _schedule[@ SGMS_SCHEDULE.TARGET] = argument1;
	}


}
