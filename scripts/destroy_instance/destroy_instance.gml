/// @description destroy_instance(instance)
/// @function destroy_instance
/// @param instance
function destroy_instance(argument0) {
	if(argument0 != noone && instance_exists(argument0)) {
	    with(argument0) {
	        instance_destroy();
	    } 
	}



}
