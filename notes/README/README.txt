#Description:
	This tiny extension for android devices, terminates your app without sending it to background (default behavior).
	Boilerplate code aside, this extension is only a single line of code that commands the device to terminate the app
	natively. The package "com.yoyogames.runner.RunnerJNILib" it is not imported, so I believe this is the most tiny and
	lightweight native android extension that could be created for GameMaker: Studio 2. 
	
	Free the using resources of your user’s device immediately after exiting your app and prolong the battery duration.

#Instructions:
	Simply import the extension and then put in your quit/exit part of your code :

		android_game_end();
		
	With update v1.0.4 you can alternatively minimize you game and send it to background processes:
	
		android_game_minimize();	

#PLEASE NOTE: 
	- This extension is intended strictly for android phones and tablets ONLY.
	- This extension has not tested on Amazon Fire
	- If you develop cross platform, better use this extension conditional.
	  Examples:

		if os_type == os_android then android_game_end();
		
		if keyboard_check_pressed(vk_backspace) and os_type == os_android then android_game_minimize();	

	- Google is not suggesting terminating the app by the developer (android_game_end) and this is considered as a bad 
	practice and instead is suggesting to send the to background (android_game_minimize) and let the os take care of it.
	Since is not an obligation, developer should have to option to terminate the app if he wish so. 
	
#Even if this extension is completely free and CC0, please read the LICENSE before using it.

